package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    public DatePicker brithday;
    public RadioButton rbM;
    public ToggleGroup sexGroup;
    public RadioButton rbF;
    public RadioButton rbFree;
    public Button btnSelectAvatar;
    public ImageView myAvatar;
    public GridPane mycontroller;
    public ComboBox<String> cbBlood;


    public void doSelectBrithday(ActionEvent actionEvent) {
        System.out.println(brithday.getValue());
    }

    public void doSelectSex(ActionEvent actionEvent) {
        RadioButton radioButton = (RadioButton)actionEvent.getSource();
        System.out.println(radioButton.getText());
    }

    public void doSelectAvatar(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("影像檔","*.png","*.jpg","*.gir"));
        File selectedFile = fileChooser.showOpenDialog(mycontroller.getScene().getWindow());
        System.out.println(selectedFile.getAbsolutePath());
        myAvatar.setImage(new Image(selectedFile.toURI().toString()));
    }
    @Override
    public void initialize(URL location, ResourceBundle resources){
        ObservableList<String> items = FXCollections.observableArrayList();
        items.addAll("A","O","P");
        cbBlood.getItems().addAll(items);



    }
}

